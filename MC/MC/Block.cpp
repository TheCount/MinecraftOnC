#include "Block.h"
#include "Mesh.h"
#include "Texture.h"
#include "MeshShader.h"
#include "Camera.h"
#include <iostream>

static MeshShader* shader;
static Camera* camera;
static Mesh mesh;

static std::vector<Vertex> blockVert = {
	Vertex{ glm::vec3(0,1,0), glm::vec2(0,0) },//0 First pair
	Vertex{ glm::vec3(0,0,0), glm::vec2(0,1) },//1
	Vertex{ glm::vec3(1,0,0), glm::vec2(1,1) },//2
	Vertex{ glm::vec3(1,1,0), glm::vec2(1,0) },//3
	Vertex{ glm::vec3(0,1,1), glm::vec2(0,0) },//4
	Vertex{ glm::vec3(0,0,1), glm::vec2(0,1) },//5
	Vertex{ glm::vec3(1,0,1), glm::vec2(1,1) },//6
	Vertex{ glm::vec3(1,1,1), glm::vec2(1,0) },//7

	Vertex{ glm::vec3(0,1,0), glm::vec2(1,0) },//0 Second pair
	Vertex{ glm::vec3(0,0,0), glm::vec2(1,1) },//1
	Vertex{ glm::vec3(1,0,0), glm::vec2(1,1) },//2
	Vertex{ glm::vec3(1,1,0), glm::vec2(1,0) },//3
	Vertex{ glm::vec3(0,1,1), glm::vec2(0,0) },//4
	Vertex{ glm::vec3(0,0,1), glm::vec2(0,1) },//5
	Vertex{ glm::vec3(1,0,1), glm::vec2(0,1) },//6
	Vertex{ glm::vec3(1,1,1), glm::vec2(0,0) },//7

	Vertex{ glm::vec3(0,1,0), glm::vec2(0,1) },//0 Third pair
	Vertex{ glm::vec3(0,0,0), glm::vec2(0,1) },//1
	Vertex{ glm::vec3(1,0,0), glm::vec2(1,1) },//2
	Vertex{ glm::vec3(1,1,0), glm::vec2(1,1) },//3
	Vertex{ glm::vec3(0,1,1), glm::vec2(0,0) },//4
	Vertex{ glm::vec3(0,0,1), glm::vec2(0,0) },//5
	Vertex{ glm::vec3(1,0,1), glm::vec2(1,0) },//6
	Vertex{ glm::vec3(1,1,1), glm::vec2(1,0) }//7
};
static std::vector<GLuint> blockInd = {
	2, 1, 0,
	2, 0, 3,

	4, 5, 6,
	7, 4, 6,

	14, 10, 11,
	14, 11, 15,

	8, 9, 13,
	12, 8, 13,

	17, 18, 22,
	21, 17, 22,

	19, 16, 20,
	19, 20, 23
};

Block::Block(MATERIAL type) {

	tex = Material::toTexture(type);
	shader = MeshShader::getGlobal(0);
	camera = Camera::getGlobal(0);
	if (mesh.getVAO() == NULL) {
		mesh.Init(blockVert, blockInd);
	}

}

Location Block::getLocation() {
	Location loc;
	loc.x = (int)transform.GetPos().x;
	loc.y = (int)transform.GetPos().y;
	loc.z = (int)transform.GetPos().z;
	return loc;
}

void Block::setLocation(Location loc) {
	transform.SetPos(glm::vec3(loc.x, loc.y, loc.z));
}

void Block::setLocation(int x, int y, int z) {
	transform.SetPos(glm::vec3(x, y, z));
}

void Block::rotate(double x, double y, double z) {
	transform.GetRot() += glm::vec3(x, y, z);
}

void Block::setRotation(double x, double y, double z) {
	transform.GetRot() = glm::vec3(x, y, z);
}

void Block::teleport(double x, double y, double z) {
	transform.GetPos() = glm::vec3(x, y, z);
}

void Block::move(double x, double y, double z) {
	transform.GetPos() += glm::vec3(x, y, z);
}

void Block::Draw() {
	shader->Update(transform.GetMatrix(), camera->GetViewMatrix());
	tex.Bind();
	mesh.Draw();
}