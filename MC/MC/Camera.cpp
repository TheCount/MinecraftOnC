#include "Camera.h"
#include <algorithm>

#define RIGHT_ANGLE		1.553343034274953323f

static std::vector<Camera*> globals;

Camera* Camera::getGlobal(int index) {
	return globals[index];
}

int Camera::globalIndex() {
	for (unsigned int i = 0; i < globals.size(); i++) {
		if (globals[i] == this) {
			return i;
		}
	}
	return 0;
}

Camera::Camera(const glm::vec3& pos, double fov, double aspect, double zNear, double zFar) {
	globals.push_back(this);

	m_perspective = glm::perspective(fov, aspect, zNear, zFar);
	m_position = pos;
	m_forward = glm::vec3(0, 0, -1);
	m_up = glm::vec3(0, 1, 0);
	m_rot = 0;
	m_yaw = 0;
	m_fov = fov; m_aspect = aspect; m_znear = zNear; m_zfar = zFar;
	transform.SetPos(glm::vec3(0, 0, 0));
	transform.SetScale(glm::vec3(1, 1, 1));
}

Camera::~Camera() {
	globals.erase(std::remove(globals.begin(), globals.end(), this), globals.end());
}

void Camera::Rotate(glm::vec3 amt) {
	glm::vec3& ref = transform.GetRot();
	ref += amt;
	if (ref.x > RIGHT_ANGLE) { ref.x = RIGHT_ANGLE; }
	if (ref.x < -RIGHT_ANGLE) { ref.x = -RIGHT_ANGLE; }
	m_forward = glm::vec3(transform.GetMatrix() * glm::vec4(0, 0, 1, 1));
}

void Camera::Move(double f, double r) {//Forward, Right relative to viewing angle
	double rad;
	if (f != 0) {
		rad = (m_rot * PI / 180);
		m_position.x += float(cos(rad) * f);
		m_position.z += float(sin(rad) * f);
	}
	if (r != 0) {
		rad = ((m_rot + 90) * PI / 180);
		m_position.x += float(cos(rad) * r);
		m_position.z += float(sin(rad) * r);
	}
}

void Camera::Teleport(double x, double y, double z) {
	m_position.x = (float)x;
	m_position.y = (float)y;
	m_position.z = (float)z;
}