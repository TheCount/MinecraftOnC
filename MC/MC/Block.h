#pragma once

#include "Mesh.h"
#include "Material.h"
#include "Transform.h"

struct Location {
	int x;
	int y;
	int z;
};

class Block {
public:

	Block(MATERIAL type);

	void rotate(double x, double y, double z);
	void setRotation(double x, double y, double z);

	void teleport(double x, double y, double z);
	void move(double x, double y, double z);

	Location getLocation();
	void setLocation(Location loc);
	void setLocation(int x, int y, int z);

	void Draw();

private:
	Texture tex;
	Transform transform;
};